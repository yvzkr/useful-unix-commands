# Useful Unix Commands
Bu depo altında her an kullanmaya ihtiyaç duyabileceğiniz komutlar paylaşılacaktır. Sizde aynı formatta kendi komutlarınızı gönderebilirsiniz.

## Kategoriler
- [Network](#network)


### Network
- Portun hangi uygulama tarafından kullanıldığını görmek için `netstat -tulpn` kullanılabilir.   
  Örnek kullanım;
  ```bash
  $  netstat -tulpn | grep 3000
  >> tcp        0      0 0.0.0.0:3000            0.0.0.0:*               DİNLE      16593/puma 3.12.0
  ```